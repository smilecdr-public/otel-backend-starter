#!/usr/bin/env bash

# This is a script to start a choice of otel backend containers together with otel collector.
# This is achieved using the 'profiles' feature of 'docker compose'.
# In the compose file (docker-compose-with-otel-collector-choices.yaml), each container has an associated profile
# (except for otel-collector container which is run always).
# This script takes the profile names as arguments and passes them to the 'docker compose up'.

set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

function join_by { local IFS="$1"; shift; echo "$*"; }

PROFILE_ARGS=""

# an array to store the selected trace exporter names, which will be used in otel-collector config
declare -a TRACE_EXPORTERS_ARR=()
# an array to store the selected log exporter names, which will be used in otel-collector config
declare -a LOG_EXPORTERS_ARR=()

while [[ $# -gt 0 ]]; do
  case $1 in
    prometheus)
        PROFILE_ARGS="$PROFILE_ARGS --profile prometheus"
        shift # past argument
        ;;
    jaeger)
      TRACE_EXPORTERS_ARR+=('"otlp/jaeger"')
      PROFILE_ARGS="$PROFILE_ARGS --profile jaeger"
      shift # past argument
      ;;
    tempo)
      TRACE_EXPORTERS_ARR+=('"otlp/tempo"')
      PROFILE_ARGS="$PROFILE_ARGS --profile tempo"
      shift # past argument
      ;;
    grafana)
      PROFILE_ARGS="$PROFILE_ARGS --profile grafana"
      shift # past argument
      ;;
    loki)
      PROFILE_ARGS="$PROFILE_ARGS --profile loki"
      LOG_EXPORTERS_ARR+=('"otlphttp/loki"')
      shift # past argument
      ;;
   *)
      echo "Unknown argument $1"
      exit 1
      ;;
  esac
done

if [[ -z ${TRACE_EXPORTERS_ARR[@]} ]]; then
  #trace exporters array is empty, add the debug so that otel collector doesn't complain
  TRACE_EXPORTERS_ARR+=('"debug"')
fi
# create an env variable to hold selected trace exporters in a yaml array string,
# this is used in otel collector config
TRACE_EXPORTERS=$(join_by ',' ${TRACE_EXPORTERS_ARR[@]})
TRACE_EXPORTERS="["$TRACE_EXPORTERS"]"
export TRACE_EXPORTERS


if [[ -z ${LOG_EXPORTERS_ARR[@]} ]]; then
  #log exporters array is empty, add the debug so that otel collector doesn't complain
  LOG_EXPORTERS_ARR+=('"debug"')
fi
# create an env variable to hold selected log exporters in a yaml array string,
# this is used in otel collector config
LOG_EXPORTERS=$(join_by ',' ${LOG_EXPORTERS_ARR[@]})
LOG_EXPORTERS="["$LOG_EXPORTERS"]"
export LOG_EXPORTERS

docker compose -f "${SCRIPT_DIR}/../docker-compose-with-otel-collector-choices.yaml" $PROFILE_ARGS up -d