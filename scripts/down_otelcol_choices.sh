#!/usr/bin/env bash
# This is a script to take down all containers that might have been started
# by the up_otelcol_choices.sh

set -e
# find out the dir where this script is located
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# call 'compose down' with all available profiles to take down all containers
# including a profile that isn't currently active is harmless
docker compose -f "${SCRIPT_DIR}/../docker-compose-with-otel-collector-choices.yaml"\
 --profile jaeger\
 --profile grafana\
 --profile tempo\
 --profile prometheus\
 --profile loki\
 down
